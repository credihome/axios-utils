var axios = require('axios');
var parseErrorAxios = require('./index');

describe('test parseErrorAxios', () => {
  it('test url and statusCode', async () => {
    const url = "https://api-site.credihome.com.br/staging/termsofservice/whitelabel";
    const data = {};
    await axios.post(url, data).catch(err => {
      const error = parseErrorAxios(err);
      expect(error.url).toEqual(url);
      expect(error.response.statusCode).toEqual(500);
    });
  });

  it('test data empty and statusCode', async () => {
    const url = "https://api-partner.credihome.com.br/staging/zipcode/01254751";
    await axios.get(url).catch(err => {
      const error = parseErrorAxios(err);
      expect(error.data).toEqual('');
      expect(error.response.statusCode).toEqual(404);
      expect(error.response.statusText).toEqual('Not Found');
    });
  });

  it('test unauthoried request', async () => {
    const url = "https://api-partner.credihome.com.br/staging/leads/";
    await axios.get(url).catch(err => {
      const error = parseErrorAxios(err);
      expect(error.data).toEqual('');
      expect(error.response.statusCode).toEqual(undefined);
    });
  })
})
