module.exports = parseErrorAxios;

function parseErrorAxios(error) {
  const objerror = {
    url: error.config.url,
    method: error.config.method,
    request: {
      headerRequest: error.config.headers,
    },
    response: {
      statusCode: error.response ? error.response.status : undefined,
      statusText: error.response ? error.response.statusText : undefined,
      headerResponse: error.response ? error.response.headers : undefined,
      errorMessages: error.response ? error.response.data : undefined,
    },
    data: error.config.data !== undefined ? JSON.parse(error.config.data) : '',
  };
  return objerror;
}