# Axios Utils

## Como instalar
Para adicionar esse módulo em outro projeto é só rodar o código abaixo no repositório.

```
yarn add https://bitbucket.org/credihome/axios-utils.git#VERSION
```

### `parseErrorAxios`
Essa função recebe um erro do catch do axios e retorna um objeto com as principais informações do erro. Um exemplo de objeto de saída:

```javascript
  {
    url: 'https://api-site.credihome.com.br/staging/termsofservice/whitelabel',
    method: 'post',
    request: {
      headerRequest: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json;charset=utf-8',
      }
    },
    response: {
      statusCode: 500,
      headerResponse: {
        date: 'Mon, 10 Aug 2020 19:02:04 GMT',
        'content-type': 'application/json',
        'content-length': '62',
        connection: 'close',
        'x-amzn-requestid': '7b5423de-ac0a-44aa-afa5-cadfd8103a4f',
        'access-control-allow-origin': '*',
        'x-amz-apigw-id': 'RETy_GG1GjQFjTw=',
        'x-amzn-trace-id': 'Root=1-5f3199ac-98908b78c27479da250d5a54;Sampled=0'
      },
      errorMessages: { 
        message: "Cannot read property 'replace' of undefined" 
      }
    },
    data: {
      userAgent: 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0',
    }
  }
```
 
Caso o erro vindo do axios não apresente alguma essas informações, o atributo do objeto é setado como undefined.